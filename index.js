// deps
const config = require('./config.json');
const db = require('./db');
const scrapers = require('./scrapers');
const server = require('./server');

// create the database
db.create(config.db.name);

// run scrapers now and every N minutes
scrapers.execAll(db.getConn());
setInterval(() => scrapers.execAll(db.getConn()), config.scrapingInterval);

// server is running from now on
server.run();
