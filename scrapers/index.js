// scrapers
const worldometersScraper = require('./worldometers');

// execute all scrapers
const execAll = (db) => {
    worldometersScraper.getMainData(db);
    worldometersScraper.getGraphsData(db);
};

module.exports = {
    execAll
};
