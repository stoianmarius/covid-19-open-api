// deps
const axios = require('axios');
const cheerio = require('cheerio');
const countryUtils = require('../utils/country');
const config = require('../config.json');
const dbKeys = config.db.keys;

// base url for page that is being scraped
const BASE_URL = 'https://www.worldometers.info/coronavirus/';

// charts keys
const CHART_KEY = {
    'coronavirus-cases-linear': 'totalCases',
    'coronavirus': 'dailyNewCases',
    'graph-active-cases-total': 'activeCases',
    'graph-cured-total': 'totalCured',
    'total-serious-linear': 'totalCritical'
};
const validCharts = Object.keys(CHART_KEY);

// convert html text value to number
const parseNumberValue = (value) => {
    value = value.trim() || '0';
    return parseInt(value.replace(/,/g, '') || '0', 10);
};

// get data from table cell
const getCellData = (cell) => {
    return parseNumberValue(cell.children.length !== 0 ? cell.children[0].data : '');
};

// get country from table cell
const getCellCountryData = (cell) => {
    let country = (cell.children[0].data
        || cell.children[0].children[0].data
        // country name with link has another level
        || cell.children[0].children[0].children[0].data
        || cell.children[0].children[0].children[0].children[0].data
        || '').trim();
    if (country.length === 0) {
        // parse with hyperlink
        country = (cell.children[0].next.children[0].data || '').trim();
    }
    return country;
};

// extract data from death tables
const extractDeathTableData = (rows) => {
    const result = {};
    // first and last rows are ignored
    for (let i = rows.length - 1; i >= 0 ; i--) {
        if (rows[i].name !== 'tr') {
            continue;
        }

        const cells = rows[i];
        const category = cells.children[1].children[1].children[0].data;
        const value = parseNumberValue(cells.children[3].children[0].children[0].data);

        // category is only of type date atm
        // convert it into unix timestamp
        const month = category.substring(0, 3).trim();
        const day = category.substring(5).trim();
        const year = '2019'; // lets hope this will not change :c
        const date = new Date(`${month} ${day} ${year}`);

        result[date.getTime()] = value;
    }
    return result;
};

// extract data from total deaths and daily deaths tables per day
const parseDeathTables = (html) => {
    const totalTableRows = html('div.table-responsive:nth-child(2) > table:nth-child(1) > tbody')[0].children;
    const dailyTableRows = html('div.table-responsive:nth-child(4) > table:nth-child(1) > tbody')[0].children;

    const totalDead = extractDeathTableData(totalTableRows);
    const dailyDead = extractDeathTableData(dailyTableRows);

    return {
        totalDead,
        dailyDead
    };
};

// parse countries table html into json
const parseCountriesTable = (html) => {
    const countriesResult = [];

    // index positions of certain columns
    const countryColIndex = 0;
    const casesColIndex = 1;
    const newCasesColIndex = 2;
    const deathsColIndex = 3;
    const newDeathsColIndex = 4;
    const curedColIndex = 5;
    const activeColIndex = 6;
    const criticalColIndex = 7;
    const casesPerOneMillionColIndex = 8;
    const deathsPerOneMillionColIndex = 9;

    const rows = html('table#main_table_countries_today > tbody')[0].children
        .filter(r => r.name && r.name === 'tr' &&
            (!r.attribs || (r.attribs && r.attribs.class !=='total_row_world row_continent'))
        );

    for (let i = 1; i < rows.length; i++) {
        const result = {};

        const cells = rows[i].children.filter(c => c.name === 'td');
        for (let cIndex = 0; cIndex < cells.length; cIndex++) {
            const cell = cells[cIndex];

            // get country
            if (cIndex === countryColIndex) {
                const country = getCellCountryData(cell);
                const countryData = countryUtils.getCountryData(country);
                delete countryData.country;

                result.country = country;
                result.info = countryData;
            }
            // get cases
            if (cIndex === casesColIndex) {
                result.totalCases = getCellData(cell);
            }
            // get today cases
            if (cIndex === newCasesColIndex) {
                result.todayCases = getCellData(cell);
            }
            // get deaths
            if (cIndex === deathsColIndex) {
                result.totalDeaths = getCellData(cell);
            }
            // get yesterdays deaths
            if (cIndex === newDeathsColIndex) {
                result.todayDeaths = getCellData(cell);
            }
            // get cured
            if (cIndex === curedColIndex) {
                result.totalRecovered = getCellData(cell);
            }
            // get active
            if (cIndex === activeColIndex) {
                result.activeCases = getCellData(cell);
            }
            // get critical
            if (cIndex === criticalColIndex) {
                result.criticalCases = getCellData(cell);
            }
            // get total cases per one million population
            if (cIndex === casesPerOneMillionColIndex) {
                const casesPerOneMillion = cell.children.length !== 0 ? cell.children[0].data : '';
                result.casesPerOneMillion = parseFloat(casesPerOneMillion.split(',').join(''));
            }
            // get total deaths per one million population
            if (cIndex === deathsPerOneMillionColIndex) {
                const deathsPerOneMillion = cell.children.length !== 0 ? cell.children[0].data : '';
                result.deathsPerOneMillion = parseFloat(deathsPerOneMillion.split(',').join(''));
            }

            result.lastUpdate = Date.now();
        }

        countriesResult.push(result);
    }
    return countriesResult.sort((a, b) => (a.totalCases > b.totalCases ? -1 : 1))
};

// parse summary html block into json
const parseSummaryHtml = (html) => {
    const result = {};

    html('.maincounter-number').map((i, el) => {
        const count = parseNumberValue(el.children[0].next.children[0].data);
        switch (i) {
            case 0:
                result.totalCases = count;
                break;
            case 1:
                result.totalDeaths = count;
                break;
            default:
                result.totalRecovered = count;
                break;
        }
    });

    // get active/closed cases data
    const dataSelector = '.container > div:nth-child(2) > div.col-md-8 > div';
    const activeCasesHtml = html(`${dataSelector} > div:nth-child(14) .panel_front`)[0].children[5];
    const closedCasesHtml = html(`${dataSelector} > div:nth-child(15) .panel_front`)[0].children[5];
    result.activeCases = {
        mild: parseNumberValue(activeCasesHtml.children[1].children[1].children[0].data),
        critical: parseNumberValue(activeCasesHtml.children[3].children[0].children[0].data)
    };
    result.closedCases = {
        recovered: parseNumberValue(closedCasesHtml.children[1].children[1].children[0].data),
        dead: parseNumberValue(closedCasesHtml.children[3].children[0].children[0].data)
    };

    // data timestamp
    result.lastUpdate = Date.now();

    return result;
};

// parse highcharts script and extract values
const parseGraphsScript = (script) => {
    // find dates
    const categoriesStartToken = 'xAxis:{categories:';
    const categoriesStartIndex = script.indexOf(categoriesStartToken);
    const categoriesEndIndex = script.indexOf('},yAxis');
    const categoriesStr = script.substring(categoriesStartIndex + categoriesStartToken.length, categoriesEndIndex);
    const categoriesArr = JSON.parse(categoriesStr);

    // find values
    const valuesStartToken = 'data:';
    const valuesStartIndex = script.indexOf(valuesStartToken);
    const valuesEndIndex = script.indexOf('}],responsive');
    const valuesStr = script.substring(valuesStartIndex + valuesStartToken.length, valuesEndIndex);
    const valuesArr = JSON.parse(valuesStr).map(v => !v ? 0 : v);

    const result = {};
    categoriesArr.forEach((category, index) => {
        // atm only the categories seems to be dates
        // so we just hardcode the date parsing here, until we scrape graphs with other types of category
        // this code is pretty silly, but it works atm
        const month = category.substring(0, 3);
        const day = category.substring(3);
        const year = '2019'; // lets hope this will not change :c
        result[`${month} ${day}`] = valuesArr[index];
    });
    return result;
};

// extract values from charts
const parseCasesGraphsData = (html) => {
    const scriptsData = Object.values(html('script:not([src])'));
    const chartScripts = {};
    scriptsData.forEach(script => {
       if (Array.isArray(script.children) && script.children.length >= 1) {
           let scriptText = script.children.map(el => {
               el.data = el.data || '';
               return el.data.trim().replace(/\s/g, '')
           }).join('');
           if (scriptText.indexOf('Highcharts.chart(') !== -1) {
               // categorize the script
               validCharts.forEach(key => {
                   if (scriptText.indexOf(`Highcharts.chart('${key}'`) !== -1) {
                       // weird case, rest of the script body is in its sibling
                       if (key === 'coronavirus') {
                           scriptText += (script.children[1].children[0].data || '').trim().replace(/\s/g, '');
                       }
                       chartScripts[key] = scriptText;
                   }
               });
           }
       }
       return false;
    });

    // this goes into db
    const result = {};

    // extract graphs data and store it in database
    for (let chartKey in chartScripts) {
        result[CHART_KEY[chartKey]] = parseGraphsScript(chartScripts[chartKey]);
    }

    return result;
};

// get cases graphs and additional data
const getCasesGraphData = async () => {
    let response = null;
    try {
        response = await axios.get(`${BASE_URL}coronavirus-cases`);
    } catch (err) {
        return null;
    }

    // extract graphs data from js scripts (dirty way to do it :c)
    const html = cheerio.load(response.data, {xmlMode: true});

    return parseCasesGraphsData(html);
};

// get deaths graph data
const getDeathsGraphData = async () => {
    let response = null;
    try {
        response = await axios.get(`${BASE_URL}coronavirus-death-toll`);
    } catch (err) {
        return null;
    }

    // extract graphs data from tables
    const html = cheerio.load(response.data);

    return parseDeathTables(html);
};

// get data from worldofmeters and store it into database
const getMainData = async (db) => {
    let response = null;
    try {
        response = await axios.get(BASE_URL);
    } catch (err) {
        return null;
    }

    // extract data and store JSON into database
    const html = cheerio.load(response.data);

    await db.put(dbKeys.worldwide, JSON.stringify(parseSummaryHtml(html)));
    await db.put(dbKeys.countries, JSON.stringify(parseCountriesTable(html)));
};

// get graphs data (cases/death toll) and store it
const getGraphsData = async (db) => {
    const graphs = { ...await getCasesGraphData(), ...await getDeathsGraphData() };
    await db.put(dbKeys.graphs, JSON.stringify(graphs));
};

module.exports = {
    getMainData,
    getGraphsData
};
