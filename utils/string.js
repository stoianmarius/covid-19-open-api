const normalizeWord = (word) => {
    if (word) {
        return word.trim().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    }
    return 'null';
};

module.exports = {
    normalizeWord
};
