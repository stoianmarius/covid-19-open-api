// deps
const fs = require('fs').promises;
const countryUtils = require('./utils/country');
const stringUtils = require('./utils/string');
const db = require('./db');
const config = require('./config.json');
const dbConfig = config.db;

// set response content type to JSON
const setJsonType = (reply) => {
    reply.type('application/json; charset=utf-8');
};

// send buffer as json back to the requester
const sendBufferAsJson = (buffer, reply) => {
    setJsonType(reply);
    return reply.send(buffer.toString());
};

// API's routes to be registered on the server
const routes = [
    {
        method: 'GET',
        url: '/settings',
        handler: async (req, reply) => {
            const settings = await fs.readFile('settings.json');
            setJsonType(reply);
            return settings || '{}';
        }
    },
    {
        method: 'GET',
        url: '/worldwide',
        handler: async (req, reply) => sendBufferAsJson(await db.getConn().get(dbConfig.keys.worldwide), reply)
    },
    {
        method: 'GET',
        url: '/graphs',
        handler: async (req, reply) => sendBufferAsJson(await db.getConn().get(dbConfig.keys.graphs), reply)
    },
    {
        method: 'GET',
        url: '/countries',
        handler: async (req, reply) => sendBufferAsJson(await db.getConn().get(dbConfig.keys.countries), reply)
    },
    {
        method: 'GET',
        url: '/countries/:query',
        handler: async (req, reply) => {
            const buffer = await db.getConn().get(dbConfig.keys.countries);
            const countries = JSON.parse(buffer.toString());

            const { query } = req.params;

            const isText = isNaN(query);
            const countryInfo = isText ? countryUtils.getCountryData(query) : null;
            const standardizedCountryName = (countryInfo && countryInfo.country) ? countryInfo.country.toLowerCase() : null;

            const country = countries.find(country => {
                // ISO or name
                if (isText) {
                    return (
                        (country.info.iso3 || 'null').toLowerCase() === query.toLowerCase()
                        || (country.info.iso2 || 'null').toLowerCase() === query.toLowerCase()
                        || ((query.length > 3 || countryUtils.isCountryException(query.toLowerCase()))
                            && stringUtils.normalizeWord(country.country.toLowerCase()).includes(standardizedCountryName))
                    );
                }
                return normalizeWord.countryInfo._id === Number(query);
            });

            if (country) {
                return country;
            }

            // not found
            reply.code(404).send();
        }
    }
];

module.exports = (server, opts, done) => {
    for (let route of routes) {
        server.route(route);
    }
    return done();
};
