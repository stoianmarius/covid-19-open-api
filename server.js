// deps
const config = require('./config.json').api;
const fastify = require('fastify');

// create a server instance
const server = require('fastify')({
    logger: config.logger
});

// register plugins
server.register(require('fastify-rate-limit'), config.rateLimit);
server.register(require('fastify-cors'));

// register routes
server.register(require('./routes'));

// run the server
const run = () => {
    server.listen(process.env.PORT || config.port, config.host, (err) => {
        if (err) {
            server.log.error(err);
            process.exit(1);
        }
    });
};

module.exports = {
    run
};
