# COVID-19 Stats API

API for getting latest information about COVID-19 disease.

It works by scraping various trusted sources like Worldometers.info

When sources are changing their HTML structure this API might not work as expected :c

<a href="https://www.buymeacoffee.com/st0ian" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/lato-red.png" alt="Buy Me A Coffee" style="height: 51px !important;width: 217px !important;" ></a>


## Development
Default API port is 3000, make sure it's available when running 'npm run start', otherwise change the port in config.json.
```
git clone https://gitlab.com/st0ianm/covid-19-open-api.git
cd covid-19-open-api
npm install
cp config.example.json config.json
npm run start
```

## API Documentation

### Worldwide
Get counts about active cases, total cases, deaths worldwide.

**URL** : `/worldwide`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content examples**

```json
{
    "totalCases": 1521966,
    "totalDeaths": 88659,
    "totalRecovered": 332310,
    "activeCases": {
        "mild": 1052843,
        "critical": 48154
    },
    "closedCases": {
        "recovered": 332310,
        "dead": 88659
    },
    "lastUpdate": 1586423712545
}
```

### Graphs
Get data required to build graphs like total active cases, deaths/recovered rate pie charts.

**URL** : `/graphs`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content examples**

```json
{
    "totalCases": {
      "Jan 22": 580,
      "Jan 23": 845,
      "Jan 24": 1317,
      "Jan 25": 2015,
      "Jan 26": 2800,
      "Jan 27": 4581
    },
    "dailyNewCases": {
      "Jan 22": 580,
      "Jan 23": 845,
      "Jan 24": 1317,
      "Jan 25": 2015,
      "Jan 26": 2800,
       "Jan 27": 4581
    }
}
```

### Countries
Get data for each country.

**URL** : `/countries`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content examples**

```json
[
    {
        "country": "USA",
        "info": {
            "_id": 840,
            "iso2": "US",
            "iso3": "USA",
            "lat": 38,
            "long": -97,
            "flag": "https://gitlab.com/st0ianm/covid-19-open-api/-/raw/master/assets/country_flags/us.png"
        },
        "lastUpdate": 1586423712656,
        "totalCases": 435160,
        "todayCases": 233,
        "totalDeaths": 14797,
        "todayDeaths": 9,
        "totalRecovered": 22891,
        "activeCases": 397472,
        "criticalCases": 9279,
        "casesPerOneMillion": 1315,
        "deathsPerOneMillion": 45
    },
    {
        "country": "Spain",
        "info": {
            "_id": 724,
            "iso2": "ES",
            "iso3": "ESP",
            "lat": 40,
            "long": -4,
            "flag": "https://gitlab.com/st0ianm/covid-19-open-api/-/raw/master/assets/country_flags/es.png"
        },
        "lastUpdate": 1586423712657,
        "totalCases": 148220,
        "todayCases": 0,
        "totalDeaths": 14792,
        "todayDeaths": 0,
        "totalRecovered": 48021,
        "activeCases": 85407,
        "criticalCases": 7069,
        "casesPerOneMillion": 3170,
        "deathsPerOneMillion": 316
    }
]
```

### Country
Get data for a specific country.

**URL** : `/countries/USA`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content examples**

```json
{
    "country": "USA",
    "info": {
        "_id": 840,
        "iso2": "US",
        "iso3": "USA",
        "lat": 38,
        "long": -97,
        "flag": "https://gitlab.com/st0ianm/covid-19-open-api/-/raw/master/assets/country_flags/us.png"
    },
    "lastUpdate": 1586423712656,
    "totalCases": 435160,
    "todayCases": 233,
    "totalDeaths": 14797,
    "todayDeaths": 9,
    "totalRecovered": 22891,
    "activeCases": 397472,
    "criticalCases": 9279,
    "casesPerOneMillion": 1315,
    "deathsPerOneMillion": 45
}
```
