// deps
const levelup = require('levelup');
const leveldown = require('leveldown');

// db ref
let dbRef = null;

// create database
const create = (dbName) => {
    dbRef = levelup((leveldown(`./${dbName}`)));
};

const getConn = () => {
    return dbRef;
};

module.exports = {
    create,
    getConn
};
